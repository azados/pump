import 'package:flutter/material.dart';

import '../widgets/ui/default_title.dart';
import '../widgets/product/price_tag.dart';
import '../widgets/product/location_tag.dart';

class ProductPage extends StatelessWidget {
  final String title;
  final String imageUrl;
  final double price;
  final String description;
  final String location;

  ProductPage(
    this.title,
    this.imageUrl,
    this.price,
    this.description,
    this.location,
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back button pressed');
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(imageUrl),
              TitleDefault(title),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LocationTag(location),
                  PriceTag(price.toString()),
                ],
              ),
              Container(
                padding: EdgeInsets.all(13.0),
                child: Text(description),
              )
            ],
          ),
        ),
      ),
    );
  }
}
