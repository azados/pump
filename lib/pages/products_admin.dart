import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import './product_edit.dart';
import './product_list.dart';

class ProductsAdminPage extends StatelessWidget {
  final Function addProduct;
  final Function updateProduct;
  final Function deleteProduct;
  final List<Map<String, dynamic>> products;

  ProductsAdminPage(
      this.addProduct, this.updateProduct, this.deleteProduct, this.products);

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text('No drawer Do Tab'),
          ),
          ListTile(
            leading: Icon(Icons.local_play),
            title: Text('Explore'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/products');
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: _buildDrawer(context),
        appBar: AppBar(
          title: Text('Profile'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.list),
                    Text('My list'),
                  ],
                ),
                // icon: Icon(Icons.list),
                // text: 'My list',
              ),
              Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.add_circle_outline),
                    Text('Add new'),
                  ],
                ),
                // icon: Icon(Icons.add_circle_outline),
                // text: 'Add new',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            ProductListPage(products, updateProduct),
            ProductEditPage(addProduct: addProduct),
          ],
        ),
      ),
    );
  }
}
