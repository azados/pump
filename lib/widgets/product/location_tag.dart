import 'package:flutter/material.dart';

class LocationTag extends StatelessWidget {
  final String location;

  LocationTag(this.location);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 9.0, right: 13.0, left: 13.0),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.location_on,
            color: Colors.indigo.withOpacity(0.6),
            size: 14.0,
          ),
          Text(
            'Central Park',
          ),
        ],
      ),
    );
  }
}
