import 'package:flutter/material.dart';

import './price_tag.dart';
import './location_tag.dart';
import '../ui/default_title.dart';

class ProductCard extends StatelessWidget {
  final Map<String, dynamic> product;
  final int index;

  ProductCard(this.product, this.index);

  Widget _buildImageCard() {
    return ClipRRect(
        borderRadius: BorderRadius.circular(9.0),
        child: Image.asset(product['image']));
  }

  Widget _buildLocationPriceRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        LocationTag(product['location']),
        PriceTag(product['price'].toString()),
      ],
    );
  }

  Widget _buildBtnBar(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
            icon: Icon(Icons.local_play),
            color: Colors.grey,
            onPressed: () => Navigator.pushNamed<bool>(
                context, '/product/' + index.toString())),
        IconButton(
            icon: Icon(Icons.people_outline),
            color: Colors.grey,
            onPressed: () => Navigator.pushNamed<bool>(
                context, '/product/' + index.toString()))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(13.0),
      color: Colors.white.withOpacity(0.9),
      child: Column(
        children: <Widget>[
          _buildImageCard(),
          TitleDefault(product['title']),
          _buildLocationPriceRow(),
          _buildBtnBar(context),
        ],
      ),
    );
  }
}
