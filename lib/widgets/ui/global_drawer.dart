import 'package:flutter/material.dart';

class MyGlobalDrawer extends StatelessWidget {
  final String title;

  MyGlobalDrawer(this.title);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            // automaticallyImplyLeading: false,
            child: Text('No drawer Do Tab'),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.local_play),
            title: Text('Explore'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/products');
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Profile'),
            onTap: () {
              Navigator.pushReplacementNamed(context, 'admin');
            },
          )
        ],
      ),
    );
  }
}
